# BigData-All-Notes

<br/>

**最全的大数据面试文章指南**

**从入门到架构!**

## 目录大纲

| 功能说明       | 主要组件                                         | 了解                                         |
| -------------- | ------------------------------------------------ | -------------------------------------------- |
| 数据采集       | Flume、Logstash、Canal                           | Maxwell、Databus、NIFI                       |
| 数据同步       | DataX、Sqoop、Kettle                             | FlinkX                                       |
| 数据存储       | HDFS、HBase、Kudu、MongoDB、Elasticsearch、MySql | TiDB、IotDB、                                |
| 数据计算       | MapReduce、Hive、Spark、 Flink                   | Pig、Storm、Tez                              |
| 数据中间件     | Kafka、RabbitMQ、Redis、Apache RocketMQ          | Alluxio、ActiveMQ、、Memcached               |
| OLAP           | ClickHouse、Kylin、Druid、Presto                 | Impala、Hawq、Greenplum、Doris               |
| 任务调度       | Azkaban、Dolphinscheduler                        | Airflow、Oozie                               |
| 集群监控       | Grafana、Cloudera Manager                        | Zabbix、Ganglia、Ambari                      |
| 元数据管理     | Atlas                                            |                                              |
| 权限管理       | Ranger、Apache Sentry                            |                                              |
| 数据质量管理   | Griffin                                          |                                              |
| 集群管理       | k8s、docker                                      |                                              |
| 数据湖         | Hudi、Iceberg                                    |                                              |
| 数据可视化、BI | Echarts、Tableau、DataV                          | Superset、QuickBI、Kibana、Metabase、Davinci |

## :black_nib: 大数据Demo代码大全

1. [JavaHttp爬虫](大数据代码demo大全/00.java爬虫)
1. [JavaSE](大数据代码demo大全/00.javaSE)
1. [Hive函数、Spark算子等速查表](大数据代码demo大全/000速查表)
1. [HDFS_API](大数据代码demo大全/01.HDFS)
1. [MapReduce_API](大数据代码demo大全/02.MapReduce)
1. [Hive常用API](大数据代码demo大全/03.Hive)
1. [Zookeeper常用API](大数据代码demo大全/04.Zookeeper)
1. [Hbase常用API](大数据代码demo大全/05.HBase)
1. [Flume常用API如拦截器](大数据代码demo大全/06.Flume)
1. [Sqoop常用API](大数据代码demo大全/07.sqoop)
1. [Spark代码Demo](大数据代码demo大全/09.spark)
1. [Redis常用API](大数据代码demo大全/10.redis)
1. [kafka常用API](大数据代码demo大全/11.kafka)
1. [常用shell脚本](大数据代码demo大全/12.shell脚本)
1. [Flink代码Demo](大数据代码demo大全/13.flink)

## 一、数据采集

###  Flume

1. [Flume入门及安装教程](notes/flume/flume是什么?.md)
2. [Flume的运行机制与采集系统结构](https://wuwei.blog.csdn.net/article/details/103395436)
3. [Flume的多种采集方式](https://wuwei.blog.csdn.net/article/details/103395558)
4. [Flume实现两个agent级联采集](https://wuwei.blog.csdn.net/article/details/103404996)
5. [Flume配置高可用Flum-NG配置failover](https://wuwei.blog.csdn.net/article/details/103408860)
6. [Flume的负载均衡load balancer](https://wuwei.blog.csdn.net/article/details/103409801)
7. [Flume面试题](https://blog.csdn.net/shujuelin/article/details/89020156?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163844915116780269862546%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163844915116780269862546&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-2-89020156.pc_search_mgc_flag&utm_term=flume%E9%9D%A2%E8%AF%95%E9%A2%98&spm=1018.2226.3001.4187)



# 欢迎添加我的微信

![image-20211206111758632](https://gitee.com/wang_ao_qi/images/raw/master/mac_image/202112061117694.png)

<div align="center">
	<a href = "https://blog.csdn.net/weixin_43893397"> 
	<img width="200px" src="https://gitee.com/wang_ao_qi/images/raw/master/mac_image/202112022057321"/> 
	</a> 
</div>
<div align="center"> <a  href = "https://blog.csdn.net/m0_37809146"> 欢迎添加我的微信</a>
<a  href = "https://blog.csdn.net/m0_37809146"> 点击进入我的博客</a></div>

