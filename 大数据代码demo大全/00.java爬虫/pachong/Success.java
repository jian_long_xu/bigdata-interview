package com.czxy.mapreduce.pachong;

import lombok.Data;

/**
 * @author 550894211@qq.com
 * @version v 1.0
 * @date 2020/1/2
 */
@Data
public class Success {
    private Result result;
    private Boolean success;
}
